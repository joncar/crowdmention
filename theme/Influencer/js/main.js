$('.nav-btn').click(function(){
    $('.da-sidebar').toggleClass('is-closed');
    $('.da-main').toggleClass('is-open');
});

// Sliders
var slider = document.getElementById('sliderRegular');
noUiSlider.create(slider, {
	start: 40,
	connect: [true,false],
	range: {
		min: 0,
		max: 100
	}
});
