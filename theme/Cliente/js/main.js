$('.nav-btn').click(function(){
    $('.da-sidebar').toggleClass('is-closed');
    $('.da-main').toggleClass('is-open');
});

// Sliders
var slider1 = document.getElementById('sliderRegular');
noUiSlider.create(slider1, {
	start: [ 20, 60 ],
	connect: true,
	range: {
		min:  0,
		max:  100
	}	
});

var slider2 = document.getElementById('sliderRegular1');
noUiSlider.create(slider2, {
	start: [ 20, 60 ],
	connect: true,
	range: {
		min:  0,
		max:  100
	}
});

var slider3 = document.getElementById('sliderRegular2');
noUiSlider.create(slider3, {
	start: [ 20, 60 ],
	connect: true,
	range: {
		min:  0,
		max:  100
	}
});

var slider4 = document.getElementById('sliderDouble3');
noUiSlider.create(slider4, {
	start: [ 20, 60 ],
	connect: true,
	range: {
		min:  0,
		max:  100
	}
});
