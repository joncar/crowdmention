<!Doctype html>
<html lang="es">
    <head>
        <?php if(empty($crud) || empty($css_files) || !empty($loadJquery)): ?>            
        <script src="http://code.jquery.com/jquery-2.1.0.js"></script>      
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>                
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
        <?php endif ?>
        <?php 
        if(!empty($css_files) && !empty($js_files)):
        foreach($css_files as $file): ?>
        <link type="text/css" rel="stylesheet" href="<?= $file ?>" />
        <?php endforeach; ?>
        <?php foreach($js_files as $file): ?>
        <script src="<?= $file ?>"></script>
        <?php endforeach; ?>                
        <?php endif; ?>

    </head>  
    <?php $this->load->view($view) ?>
</html>
