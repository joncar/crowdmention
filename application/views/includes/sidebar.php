<?php if($this->user->log): ?>
<div id="sidebar" class="sidebar responsive">
        <ul class="nav nav-list">
            <li>
                <a href="<?= site_url('panel') ?>">
                        <i class="menu-icon fa fa-tachometer"></i>
                        <span class="menu-text">Escritorio</span>
                </a>
                <b class="arrow"></b>
            </li>
             <!--- Alumnos --->
             <?php 
                    $menu = array(
                        'boxes'=>array('admin/empleados','admin/vehiculos','admin/servicios'),
                        'maestras'=>array('tipo_contador','condiciones', 'estados', 'proveedores','paises','ciudades','proveedores','sucursales','clientes','monedas','motivo_salida','motivo_entrada','tipo_proveedores','cuentas','boxes/marcas','boxes/modelos','boxes/tipo_combustible','boxes/color_vehiculo'),
                        'cajas'=>array('admin/cajas','admin/cajadiaria','admin/gastos','admin/pagocliente','admin/pagoproveedores','admin/saldos','admin/saldos_proveedores'),
                        'movimientos'=>array('productos/categorias','productos/productos','productos/inventario','compras/compras','ventas/ventas','entradas/entrada_productos','salidas/salidas','productos/transferencias','creditos/creditos'),
                        'reportes'=>array(
                            'report',
                            'verReportes','rep/newreportes','rep/report_organizer','rep/mis_reportes',                           
                        ),
                        'seguridad'=>array('menus','ajustes','grupos','funciones','user','acciones')
                    );
                    $menu = $this->user->filtrarMenu($menu);
                    $label = array(
                        'boxes'=>array('Servicios','fa fa-wrench'),
                        'cajas'=>array('Cajas','fa fa-ticket'),
                        'productosucursal'=>array('Inventario',''),
                        'movimientos'=>array('Movimientos','fa fa-exchange'),
                         'reportes'=>array('Reportes','fa fa-files-o'),
                        'maestras'=>array('Archivo','fa fa-table'),
                        'seguridad'=>array('Seguridad','fa fa-user-secret')
                    );
             ?>
             <?php  echo getMenu($menu,$label); ?>            
        </ul>
       <div id="sidebar-collapse" class="sidebar-toggle sidebar-collapse">
            <i data-icon2="ace-icon fa fa-angle-double-right" data-icon1="ace-icon fa fa-angle-double-left" class="ace-icon fa fa-angle-double-left"></i>
        </div>
        <div style="color:white; background:#222222; font-size:8px; text-align:center">
            <a href="#" style="color:white;">
                <?= img('img/eva-01.svg','width:50%') ?>
            </a>
        </div>

        <script type="text/javascript">
                try{ace.settings.check('sidebar' , 'collapsed')
                ace.settings.sidebar_collapsed(true, true);
                }catch(e){}
        </script>
</div>
<?php endif ?>
